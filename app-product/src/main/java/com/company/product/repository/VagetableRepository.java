package com.company.product.repository;

import com.company.product.entity.Vagetable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VagetableRepository extends JpaRepository<Vagetable,Integer> {

}
