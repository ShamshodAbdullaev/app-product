package com.company.product.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TvReq {
    private String name;
    private Double price;
    private Double diaganal;
    private String brand;
    private String warranty;
    private String madeIn;
    private String tvNumber;
}
