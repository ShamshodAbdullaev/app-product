package com.company.product.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhoneReq {
    private String name;
    private Double price;
    private String brand;
    private String color;
    private String warranty;
    private String madeIn;
    private String phoneNumber;
}
