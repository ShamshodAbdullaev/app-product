package com.company.product.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class FruitReq {
    private String name;
    private Double price;
    private String type;
    private String expireDate;
    private Double colorie;
    private String madeIn;
    private String fruitNumber;
}
