package com.company.product.controller;

import com.company.product.entity.Phone;
import com.company.product.model.Result;
import com.company.product.payload.PhoneReq;
import com.company.product.repository.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/phone")
public class PhoneController {
    @Autowired
    PhoneRepository phoneRepository;

    @GetMapping("/")
    public String phonePage(){
        return "phone";
    }


//    SUBMIT ORQALI QO'SHISH

//    @PostMapping("/add")
//    public void addProduct(HttpServletRequest request, Model model){
//        Phone phone = new Phone();
//       phone.setName(request.getParameter("name"));
//       phone.setPrice(Double.valueOf(request.getParameter("price")));
//       phone.setBrand(request.getParameter("brand"));
//       phone.setColor(request.getParameter("color"));
//       phone.setWarranty(request.getParameter("warranty"));
//       phone.setMadeIn(request.getParameter("madeIn"));
//       phone.setPhoneNumber(request.getParameter("phoneNumber"));
//       Phone savedPhone = phoneRepository.save(phone);
//        if (savedPhone != null){
//            System.out.println(savedPhone+" saqlandi!!!");
//        }else {
//            System.out.println(savedPhone+" saqlanmadi!!!");
//        }
//    }

//    AJAX ORQALI  QO'SHISH

    @PostMapping("/add")
    @ResponseBody
    public Result addPhone(@RequestBody PhoneReq phoneReq){
        Result result = new Result();
        Phone phone = new Phone();
        phone.setName(phoneReq.getName());
        phone.setPrice(Double.valueOf(phoneReq.getPrice()));
        phone.setBrand(phoneReq.getBrand());
        phone.setColor(phoneReq.getColor());
        phone.setWarranty(phoneReq.getWarranty());
        phone.setMadeIn(phoneReq.getMadeIn());
        phone.setPhoneNumber(phoneReq.getPhoneNumber());
        Phone savedPhone = phoneRepository.save(phone);
        if (savedPhone!=null){
            result.setSuccess(true);
            result.setMessage("Successfully added!!!");
        }else {
            result.setSuccess(false);
            result.setMessage("Error in adding!!!");
        }
        return result;
    }

    @PutMapping("/edit/{id}")
    @ResponseBody
    public Result editPhone(@PathVariable Integer id,@RequestBody PhoneReq phoneReq){
        Result result = new Result();
        Optional<Phone> optional = phoneRepository.findById(id);
        if (optional.isPresent()){
            Phone phone = optional.get();
            phone.setName(phoneReq.getName());
            phone.setPrice(phoneReq.getPrice());
            phone.setBrand(phoneReq.getBrand());
            phone.setColor(phoneReq.getColor());
            phone.setWarranty(phoneReq.getWarranty());
            phone.setMadeIn(phoneReq.getMadeIn());
            phone.setPhoneNumber(phoneReq.getPhoneNumber());
            if (phoneRepository.save(phone)!=null){
                result.setSuccess(true);
                result.setMessage("Successfully edited!!!");
            }else {
                result.setSuccess(false);
                result.setMessage("Error in editing!!!");
            }
        }
        return result;
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public Result deletePhone(@PathVariable Integer id){
        Result result = new Result();
        phoneRepository.deleteById(id);
        Optional<Phone> optional = phoneRepository.findById(id);
        if (optional.isPresent()){
            result.setSuccess(false);
            result.setMessage(optional.get().getName()+"o`chmadi");
        }else {
            result.setSuccess(true);
            result.setMessage("Successfully deleted!!!");
        }
        return result;
    }

    @GetMapping("/list")
    @ResponseBody
    public List<Phone> getPhones(){
        return phoneRepository.findAll();
    }
}
