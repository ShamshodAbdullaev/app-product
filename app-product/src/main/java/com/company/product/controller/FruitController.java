package com.company.product.controller;

import com.company.product.entity.Fruit;
import com.company.product.model.Result;
import com.company.product.payload.FruitReq;
import com.company.product.repository.FruitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/fruit")
public class FruitController {
    @Autowired
    FruitRepository fruitRepository;

    @GetMapping("/")
    public String fruitPage(){
        return "fruit";
    }

    @PostMapping("/add")
    @ResponseBody
    public Result addFruit(@RequestBody FruitReq fruitReq){
        Result result = new Result();
        Fruit fruit = new Fruit();
        fruit.setName(fruitReq.getName());
        fruit.setPrice(Double.valueOf(fruitReq.getPrice()));
        fruit.setType(fruitReq.getType());
        fruit.setExpireDate(fruitReq.getExpireDate());
        fruit.setColorie(Double.valueOf(fruitReq.getColorie()));
        fruit.setMadeIn(fruitReq.getMadeIn());
        fruit.setFruitNumber(fruitReq.getFruitNumber());
        Fruit savedFruit = fruitRepository.save(fruit);
        if (savedFruit!=null){
            result.setSuccess(true);
            result.setMessage("Successfully added!!!");
        }else {
            result.setSuccess(false);
            result.setMessage("Error in adding!!!");
        }
        return result;
    }

    @PutMapping("/edit/{id}")
    @ResponseBody
    public Result editfruit(@PathVariable Integer id, @RequestBody FruitReq fruitReq){
        Result result=new Result();
        Optional<Fruit> optional=fruitRepository.findById(id);
        if (optional!=null){
            Fruit fruit=optional.get();
            fruit.setName(fruitReq.getName());
            fruit.setPrice(Double.valueOf(fruitReq.getPrice()));
            fruit.setType(fruitReq.getType());
            fruit.setExpireDate(fruitReq.getExpireDate());
            fruit.setColorie(Double.valueOf(fruitReq.getColorie()));
            fruit.setMadeIn(fruitReq.getMadeIn());
            fruit.setFruitNumber(fruitReq.getFruitNumber());
            if (fruitRepository.save(fruit)!=null){
                result.setSuccess(true);
                result.setMessage("Successfully edited!!!");
            }else {
                result.setSuccess(false);
                result.setMessage("Error in editing!!!");
            }
        }
        return result;
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public Result deleteFruit(@PathVariable Integer id){
        Result result = new Result();
        fruitRepository.deleteById(id);
        Optional<Fruit> optional =fruitRepository.findById(id);
        if (optional.isPresent()){
            result.setSuccess(false);
            result.setMessage(optional.get().getName()+"o`chmadi");
        }else {
            result.setSuccess(true);
            result.setMessage("Successfully deleted!!!");
        }
        return result;
    }

    @GetMapping("/list")
    @ResponseBody
    public List<Fruit> fruitsPage(){
        return fruitRepository.findAll();
    }
}
