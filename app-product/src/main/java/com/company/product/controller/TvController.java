package com.company.product.controller;

import com.company.product.entity.Tv;
import com.company.product.model.Result;
import com.company.product.payload.TvReq;
import com.company.product.repository.TvRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/tv")
public class TvController {
    @Autowired
    TvRepository tvRepository;

    @GetMapping("/")
    public String tvPage(){
        return "tv";
    }

    @PostMapping("/add")
    @ResponseBody
    public Result addTvPage(@RequestBody TvReq tvReq){
        Result result=new Result();
        Tv tv = new Tv();
        tv.setName(tvReq.getName());
        tv.setPrice(Double.valueOf(tvReq.getPrice()));
        tv.setDiaganal(Double.valueOf(tvReq.getDiaganal()));
        tv.setBrand(tvReq.getBrand());
        tv.setWarranty(tvReq.getWarranty());
        tv.setMadeIn(tvReq.getMadeIn());
        tv.setTvNumber(tvReq.getTvNumber());
        Tv savedTv =tvRepository.save(tv);
        if (savedTv!=null){
            result.setSuccess(true);
            result.setMessage("Successfully added!!!");
        }else {
            result.setSuccess(false);
            result.setMessage("Error in adding!!!");
        }
        return result;
    }


    @PutMapping("/edit/{id}")
    @ResponseBody
    public Result editTvPage(@PathVariable Integer id, @RequestBody TvReq tvReq){
        Result result=new Result();
        Optional<Tv> optional = tvRepository.findById(id);
        if (optional.isPresent()){
            Tv tv = optional.get();
            tv.setName(tvReq.getName());
            tv.setPrice(Double.valueOf(tvReq.getPrice()));
            tv.setDiaganal(Double.valueOf(tvReq.getDiaganal()));
            tv.setBrand(tvReq.getBrand());
            tv.setWarranty(tvReq.getWarranty());
            tv.setMadeIn(tvReq.getMadeIn());
            tv.setTvNumber(tvReq.getTvNumber());
            if (tvRepository.save(tv)!=null){
                result.setSuccess(true);
                result.setMessage("Successfully edited!!!");
            }else {
                result.setSuccess(false);
                result.setMessage("Error in editing!!!");
            }
        }
        return result;
    }


    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public Result deleteTv(@PathVariable Integer id){
        Result result =new Result();
        tvRepository.deleteById(id);
        Optional<Tv> optional =tvRepository.findById(id);
        if (optional.isPresent()){
            result.setSuccess(false);
            result.setMessage(optional.get().getName()+"o`chmadi");
        }else {
            result.setSuccess(true);
            result.setMessage("Successfully deleted!!!");
        }
        return result;
    }

    @GetMapping("/list")
    @ResponseBody
    public List<Tv> listTvs(){
        return tvRepository.findAll();
    }

}
