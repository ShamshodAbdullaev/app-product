package com.company.product.controller;

import com.company.product.entity.User;
import com.company.product.security.CurrentUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping({"/","/cabinet"})
    public String cabinetPage(Model model, @CurrentUser User user){
        model.addAttribute("user",user);
        return "cabinet";
    }
}
