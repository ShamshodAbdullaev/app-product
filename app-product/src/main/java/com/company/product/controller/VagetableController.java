package com.company.product.controller;

import com.company.product.entity.Vagetable;
import com.company.product.model.Result;
import com.company.product.payload.VagetableReq;
import com.company.product.repository.VagetableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/vagetable")
public class VagetableController {
    @Autowired
    VagetableRepository vagetableRepository;

    @GetMapping("/")
    public String vagetablePage(){
        return "vagetable";
    }

    @PostMapping("/add")
    @ResponseBody
    public Result addVagetablePage(@RequestBody VagetableReq vagetableReq){
        Result result = new Result();
        Vagetable vagetable= new Vagetable();
        vagetable.setName(vagetableReq.getName());
        vagetable.setPrice(Double.valueOf(vagetableReq.getPrice()));
        vagetable.setType(vagetableReq.getType());
        vagetable.setExpireDate(vagetableReq.getExpireDate());
        vagetable.setColorie(Double.valueOf(vagetableReq.getColorie()));
        vagetable.setMadeIn(vagetableReq.getMadeIn());
        vagetable.setVagetableNumber(vagetableReq.getVagetableNumber());
        Vagetable savedVagetable = vagetableRepository.save(vagetable);
        if (savedVagetable!=null){
            result.setSuccess(true);
            result.setMessage("Successfully added!!!");
        }else {
            result.setSuccess(false);
            result.setMessage("Error in adding!!!");
        }
        return result;
    }

    @PutMapping("/edit/{id}")
    @ResponseBody
    public Result editVagetable(@PathVariable Integer id,@RequestBody VagetableReq vagetableReq){
        Result result = new Result();
        Optional<Vagetable> optional = vagetableRepository.findById(id);
        if (optional!=null){
            Vagetable vagetable=optional.get();
            vagetable.setName(vagetableReq.getName());
            vagetable.setPrice(Double.valueOf(vagetableReq.getPrice()));
            vagetable.setType(vagetableReq.getType());
            vagetable.setExpireDate(vagetableReq.getExpireDate());
            vagetable.setColorie(Double.valueOf(vagetableReq.getColorie()));
            vagetable.setMadeIn(vagetableReq.getMadeIn());
            vagetable.setVagetableNumber(vagetableReq.getVagetableNumber());
            Vagetable savedVagetable = vagetableRepository.save(vagetable);
            if (savedVagetable!=null){
                result.setSuccess(true);
                result.setMessage("Successfully edited!!!");
            }else {
                result.setSuccess(false);
                result.setMessage("Error in editing!!!");
            }
        }
        return result;
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public Result deleteVagetable(@PathVariable Integer id){
        Result result = new Result();
        vagetableRepository.deleteById(id);
        Optional<Vagetable> optional = vagetableRepository.findById(id);
        if (optional.isPresent()){
            result.setSuccess(false);
            result.setMessage(optional.get().getName()+"o`chmadi");
        }else {
            result.setSuccess(true);
            result.setMessage("Successfully deleted!!!");
        }
        return result;
    }


    @GetMapping("/list")
    @ResponseBody
    public List<Vagetable> vagetablesPage(){
        return vagetableRepository.findAll();
    }
}
