$(document).ready(function () {

   var isEdit;

   var fruitTable = $('#fruitTable').DataTable({
       ajax: {
           url: "/fruit/list",
           type: "GET",
           dataSrc: ''
       },
       columns: [
           {title: "ID", data: "id"},
           {title: "Name", data: "name"},
           {title: "Price", data: "price"},
           {title: "Type", data: "type"},
           {title: "expireDate", data: "expireDate"},
           {title: "colorie", data: "colorie"},
           {title: "MadeIn", data: "madeIn"},
           {title: "fruitNumber", data: "fruitNumber"},
           {
               title: "Action", data: "id", render: function (data, type, row) {
                   return '<div class="row">\n' +
                       '    <div class="col">\n' +
                       '        <button  type="button" class="btn btn-primary text-white btn-block btn-edit"><i class="fas fa-edit"></i></button>\n' +
                       '    </div>\n' +
                       '    <div class="col">\n' +
                       '        <button data-toggle="modal" data-target="#fruitDeleteModal" class=" text-white btn btn-danger btn-block btn-delete" style="color: darkred"><i class="far fa-trash-alt"></i></button>\n' +
                       '    </div>\n' +
                       '</div>\n';
               }
           }
       ]
   });

   var fruitId = 0;
   $(fruitTable.table().body()).on('click', '.btn-delete', function () {
       var data = fruitTable.row($(this).parents('tr')).data();
       fruitId = data.id;
       $('#fruitDeleteModal').modal('toggle');
   });

   $('#fruitDelBtn').click(function () {
       $.ajax({
           url: "/fruit/delete/"+ fruitId,
           contentType: "application/json; charset=utf-8",
           method: 'DELETE',
           dataType: 'json',
           success: function (result) {
               if (result.success) {
                   $.toast({
                       heading: 'Information',
                       text: result.message,
                       icon: 'success',
                       position: 'top-right'
                   });
               } else {
                   $.toast({
                       heading: 'information',
                       text: result.message,
                       icon: 'error',
                       position: 'top-right'
                   })
               }
           },
           error: function () {
               $.toast({
                   heading: 'information',
                   text: 'Error in deleting.',
                   icon: 'error',
                   position: 'top-right'
               })
           }
       }).always(function () {
           fruitTable.ajax.reload();
           $('#fruitDeleteModal').modal('toggle');
       });
   });

   $(fruitTable.table().body()).on('click', '.btn-edit', function () {
       $('#fruitForm')[0].reset();
       var data = fruitTable.row($(this).parents('tr')).data();
       console.log(data);
       fruitId = data.id;
       isEdit=true;
       $('#fruitModal').modal('toggle');

       Object.keys(data).map(function (key) {
           $('[name=' + key + ']').val(data[key]);
       });
   });

    $('#save').click(function () {
        var fruitForm = $('#fruitForm');
        console.log(JSON.stringify(fruitForm.serializeObject()));
        $.ajax({
            url: isEdit?"/fruit/edit/"+fruitId:"/fruit/add",
            contentType: "application/json; charset=utf-8",
            method: isEdit?'PUT':'POST',
            data: JSON.stringify(fruitForm.serializeObject()),
            dataType: 'json',
            success: function (result) {
                if (result.success) {
                    $.toast({
                        heading: 'Information',
                        text: result.message,
                        icon: 'success',
                        position: 'top-right'
                    });
                } else {
                    $.toast({
                        heading: 'information',
                        text: result.message,
                        icon: 'error',
                        position: 'top-right'
                    });
                }

            },
            error: function () {
                $.toast({
                    heading: 'information',
                    text: 'Error in adding.',
                    icon: 'error',
                    position: 'top-right'
                });
            }
        }).always(function () {
                fruitTable.ajax.reload();
                $('#fruitModal').modal('toggle');
                isEdit=false;
            });
    });


    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $(this).find('input[type="hidden"], input[type="text"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select, textarea').each(function () {
            if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                var $parent = $(this).parent();
                var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                if ($chb != null) {
                    if ($chb.prop('checked')) return;
                }
            }
            if (this.name === null || this.name === undefined || this.name === '')
                return;
            var elemValue = null;
            if ($(this).is('select'))
                elemValue = $(this).find('option:selected').val();
            else elemValue = this.value;
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(elemValue || '');
            } else {
                o[this.name] = elemValue || '';
            }
        });
        return o;
    }
})