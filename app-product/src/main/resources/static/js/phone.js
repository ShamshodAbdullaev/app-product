$(document).ready(function () {

   var isEdit;

   var phoneTable = $('#phoneTable').DataTable({
       ajax: {
           url: "/phone/list",
           type: "GET",
           dataSrc: ''
       },
       columns: [
           {title: "ID", data: "id"},
           {title: "Name", data: "name"},
           {title: "Price", data: "price"},
           {title: "Brand", data: "brand"},
           {title: "Color", data: "color"},
           {title: "Warranty", data: "warranty"},
           {title: "MadeIn", data: "madeIn"},
           {title: "PhoneNumber", data: "phoneNumber"},
           {
               title: "Action", data: "id", render: function (data, type, row) {
                   return '<div class="row">\n' +
                       '    <div class="col">\n' +
                       '        <button  type="button" class="btn btn-primary text-white btn-block btn-edit"><i class="fas fa-edit"></i></button>\n' +
                       '    </div>\n' +
                       '    <div class="col">\n' +
                       '        <button data-toggle="modal" data-target="#phoneDeleteModal" class=" text-white btn btn-danger btn-block btn-delete" style="color: darkred"><i class="far fa-trash-alt"></i></button>\n' +
                       '    </div>\n' +
                       '</div>\n';
               }
           }
       ]
   });

   var phoneId = 0;
   $(phoneTable.table().body()).on('click', '.btn-delete', function () {
       var data = phoneTable.row($(this).parents('tr')).data();
       phoneId = data.id;
       $('#phoneDeleteModal').modal('toggle');
   });

   $('#phoneDelBtn').click(function () {
       $.ajax({
           url: "/phone/delete/"+ phoneId,
           contentType: "application/json; charset=utf-8",
           method: 'DELETE',
           dataType: 'json',
           success: function (result) {
               if (result.success) {
                   $.toast({
                       heading: 'Information',
                       text: result.message,
                       icon: 'success',
                       position: 'top-right'
                   });
               } else {
                   $.toast({
                       heading: 'information',
                       text: result.message,
                       icon: 'error',
                       position: 'top-right'
                   })
               }
           },
           error: function () {
               $.toast({
                   heading: 'information',
                   text: 'Error in deleting.',
                   icon: 'error',
                   position: 'top-right'
               })
           }
       }).always(function () {
           phoneTable.ajax.reload();
           $('#phoneDeleteModal').modal('toggle');
       });
   });

   $(phoneTable.table().body()).on('click', '.btn-edit', function () {
       $('#phoneForm')[0].reset();
       var data = phoneTable.row($(this).parents('tr')).data();
       console.log(data);
       phoneId = data.id;
       isEdit=true;
       $('#phoneModal').modal('toggle');

       Object.keys(data).map(function (key) {
           $('[name=' + key + ']').val(data[key]);
       });
   });

    $('#save').click(function () {
        var phoneForm = $('#phoneForm');
        console.log(JSON.stringify(phoneForm.serializeObject()));
        $.ajax({
            url: isEdit?"/phone/edit/"+phoneId:"/phone/add",
            contentType: "application/json; charset=utf-8",
            method: isEdit?'PUT':'POST',
            data: JSON.stringify(phoneForm.serializeObject()),
            dataType: 'json',
            success: function (result) {
                if (result.success) {
                    $.toast({
                        heading: 'Information',
                        text: result.message,
                        icon: 'success',
                        position: 'top-right'
                    });
                } else {
                    $.toast({
                        heading: 'information',
                        text: result.message,
                        icon: 'error',
                        position: 'top-right'
                    });
                }

            },
            error: function () {
                $.toast({
                    heading: 'information',
                    text: 'Error in adding.',
                    icon: 'error',
                    position: 'top-right'
                });
            }
        }).always(function () {
                phoneTable.ajax.reload();
                $('#phoneModal').modal('toggle');
                isEdit=false;
            });
    });


    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $(this).find('input[type="hidden"], input[type="text"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select, textarea').each(function () {
            if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                var $parent = $(this).parent();
                var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                if ($chb != null) {
                    if ($chb.prop('checked')) return;
                }
            }
            if (this.name === null || this.name === undefined || this.name === '')
                return;
            var elemValue = null;
            if ($(this).is('select'))
                elemValue = $(this).find('option:selected').val();
            else elemValue = this.value;
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(elemValue || '');
            } else {
                o[this.name] = elemValue || '';
            }
        });
        return o;
    }
})