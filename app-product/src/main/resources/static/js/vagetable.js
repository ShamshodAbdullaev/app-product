$(document).ready(function () {

   var isEdit;

   var vagetableTable = $('#vagetableTable').DataTable({
       ajax: {
           url: "/vagetable/list",
           type: "GET",
           dataSrc: ''
       },
       columns: [
           {title: "ID", data: "id"},
           {title: "Name", data: "name"},
           {title: "Price", data: "price"},
           {title: "Type", data: "type"},
           {title: "ExpireDate", data: "expireDate"},
           {title: "Colorie", data: "colorie"},
           {title: "MadeIn", data: "madeIn"},
           {title: "VagetableNumber", data: "vagetableNumber"},
           {
               title: "Action", data: "id", render: function (data, type, row) {
                   return '<div class="row">\n' +
                       '    <div class="col">\n' +
                       '        <button  type="button" class="btn btn-primary text-white btn-block btn-edit"><i class="fas fa-edit"></i></button>\n' +
                       '    </div>\n' +
                       '    <div class="col">\n' +
                       '        <button data-toggle="modal" data-target="#vagetableDeleteModal" class=" text-white btn btn-danger btn-block btn-delete" style="color: darkred"><i class="far fa-trash-alt"></i></button>\n' +
                       '    </div>\n' +
                       '</div>\n';
               }
           }
       ]
   });

   var vagetableId = 0;
   $(vagetableTable.table().body()).on('click', '.btn-delete', function () {
       var data = vagetableTable.row($(this).parents('tr')).data();
       vagetableId = data.id;
       $('#vagetableDeleteModal').modal('toggle');
   });

   $('#vagetableDelBtn').click(function () {
       $.ajax({
           url: "/vagetable/delete/"+ vagetableId,
           contentType: "application/json; charset=utf-8",
           method: 'DELETE',
           dataType: 'json',
           success: function (result) {
               if (result.success) {
                   $.toast({
                       heading: 'Information',
                       text: result.message,
                       icon: 'success',
                       position: 'top-right'
                   });
               } else {
                   $.toast({
                       heading: 'information',
                       text: result.message,
                       icon: 'error',
                       position: 'top-right'
                   })
               }
           },
           error: function () {
               $.toast({
                   heading: 'information',
                   text: 'Error in deleting.',
                   icon: 'error',
                   position: 'top-right'
               })
           }
       }).always(function () {
           vagetableTable.ajax.reload();
           $('#vagetableDeleteModal').modal('toggle');
       });
   });

   $(vagetableTable.table().body()).on('click', '.btn-edit', function () {
       $('#vagetableForm')[0].reset();
       var data = vagetableTable.row($(this).parents('tr')).data();
       console.log(data);
       vagetableId = data.id;
       isEdit=true;
       $('#vagetableModal').modal('toggle');

       Object.keys(data).map(function (key) {
           $('[name=' + key + ']').val(data[key]);
       });
   });

    $('#save').click(function () {
        var vagetableForm = $('#vagetableForm');
        console.log(JSON.stringify(vagetableForm.serializeObject()));
        $.ajax({
            url: isEdit?"/vagetable/edit/"+vagetableId:"/vagetable/add",
            contentType: "application/json; charset=utf-8",
            method: isEdit?'PUT':'POST',
            data: JSON.stringify(vagetableForm.serializeObject()),
            dataType: 'json',
            success: function (result) {
                if (result.success) {
                    $.toast({
                        heading: 'Information',
                        text: result.message,
                        icon: 'success',
                        position: 'top-right'
                    });
                } else {
                    $.toast({
                        heading: 'information',
                        text: result.message,
                        icon: 'error',
                        position: 'top-right'
                    });
                }

            },
            error: function () {
                $.toast({
                    heading: 'information',
                    text: 'Error in adding.',
                    icon: 'error',
                    position: 'top-right'
                });
            }
        }).always(function () {
                vagetableTable.ajax.reload();
                $('#vagetableModal').modal('toggle');
                isEdit=false;
            });
    });


    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $(this).find('input[type="hidden"], input[type="text"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select, textarea').each(function () {
            if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                var $parent = $(this).parent();
                var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                if ($chb != null) {
                    if ($chb.prop('checked')) return;
                }
            }
            if (this.name === null || this.name === undefined || this.name === '')
                return;
            var elemValue = null;
            if ($(this).is('select'))
                elemValue = $(this).find('option:selected').val();
            else elemValue = this.value;
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(elemValue || '');
            } else {
                o[this.name] = elemValue || '';
            }
        });
        return o;
    }
})